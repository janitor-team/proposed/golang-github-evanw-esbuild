#!/usr/bin/make -f

include /usr/share/dpkg/architecture.mk

export DH_GOLANG_INSTALL_EXTRA := \
	internal/bundler/snapshots \
	internal/resolver/testExpectations.json

export NPM_DIR := $(shell perl -e '\
  my %mapping = ( \
      amd64 => "64", i386 => "32", \
      arm64 => "arm64", armhf => "arm", \
      mips64el => "mips64le", \
      ppc64el => "ppc64le", \
      riscv64 => "riscv64", \
      s390x => "s390x", \
  ); \
  print defined($$arch = $$mapping{$(DEB_HOST_ARCH)}) ? "esbuild-linux-$$arch" : ""; \
')

%:
	dh $@ --builddirectory=_build --buildsystem=golang

ifneq ($(shell command -v node),)
execute_after_dh_auto_build-arch:
	ln -sf _build/bin/esbuild .
	# Generate npm/esbuild/*
	node scripts/esbuild.js ./esbuild --neutral
	# Generate npm/esbuild-wasm/*
	# See also set_go_env sub in dh-golang
	GOPATH=$(CURDIR)/_build GOCACHE=$(CURDIR)/_build/go-build GO111MODULE=off GOPROXY=off \
	    node scripts/esbuild.js ./esbuild --wasm

execute_after_dh_auto_test-arch:
	node -e 'require("./npm/esbuild")'
endif

ifneq ($(NPM_DIR),)
execute_after_dh_install-arch:
	cp -a npm/$(NPM_DIR) debian/esbuild/usr/lib/$(DEB_HOST_MULTIARCH)/nodejs/
	find debian/esbuild/usr/lib/$(DEB_HOST_MULTIARCH)/nodejs/ -name '*.md' -delete || true

override_dh_link-arch:
	dh_link --package=esbuild usr/bin/esbuild \
	    usr/lib/$(DEB_HOST_MULTIARCH)/nodejs/$(NPM_DIR)/bin/esbuild

override_dh_gencontrol-arch:
	dh_gencontrol -- -Vnodejs:Provides="node-$(NPM_DIR)"
endif
